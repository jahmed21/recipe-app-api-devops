terraform {
  backend "s3" {
    bucket            = "jahid-api-devops-tf-state"
    key               = "recipe.app.tfstate"
    encrypt           = true
    dynamodb_endpoint = "jahid-api-devops-tf-state-lock"
    region            = "eu-west-1"
  }
}

provider "aws" {
  region  = "eu-west-1"
  version = "~> 2.54.0"
}


locals {
  prefix      = "${var.prefix}-${terraform.workspace}"
  application = "Bastion"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}
